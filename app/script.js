(function () {
    var init = {
        page1: function () {
            $('#btn1').click(function () {
                $.get('./static/templates/page1.html', function (entry) {
                    $('#content').html(entry)
                })
            });
        },

        page2: function () {
            $('#btn2').click(function () {
                $.get('./static/templates/page2.html', function (entry) {
                    $('#content').html(entry)
                })
            });
        },

        page3: function () {
            $('#btn3').click(function () {
                $.get('./static/templates/page3.html', function (entry) {
                    $('#content').html(entry)
                })
            });
        }
    };

    $(document).ready(function () {
        init.page1();
        init.page2();
        init.page3();
    });
})();